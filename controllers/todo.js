const { TodoItem } = require('../models/todo');

class TodoController {
  static async getTodoItems(req, res) {
    try {
      const results = await TodoItem.getTodoItems();
      res.json(results);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal server error' });
    }
  }
  static async getTodoItemById(req, res) {
    try {
      const list_id = req.params.list_id;

      const item = await TodoItem.getTodoItemById(list_id);

      if (!item) {
        return res.status(404).json({ error: 'Todo item not found' });
      }

      res.json(item);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal server error' });
    }
  }
  static async createTodoItem(req, res) {
    try {
      const { list_name, description } = req.body;
      const newID = await TodoItem.createTodoItem(list_name, description);
      res.status(201).json({ list_id: newID, list_name, description });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'aaaa' });
    }
  }
  static async updateTodoItem(req, res) {
    try {
      const list_id = req.params.list_id;
      const { list_name, description } = req.body;

      const updatedRows = await TodoItem.updateTodoItem(list_id, list_name, description);

      if (updatedRows === 0) {
        return res.status(404).json({ error: 'Todo item not found' });
      }

      res.json({ message: 'Todo item updated successfully' });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal server error' });
    }
  }
  static async deleteTodoItem(req, res) {
    try {
      const list_id = req.params.list_id;

      const deletedRows = await TodoItem.deleteTodoItem(list_id);

      if (deletedRows === 0) {
        return res.status(404).json({ error: 'Todo item not found' });
      }

      res.json({ message: 'Todo item deleted successfully' });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal server error' });
    }
  }
  static async softdeleteTodoItem(req, res) {
    try {
      const list_id = req.params.list_id;

      const deletedRows = await TodoItem.softDeleteTodoItem(list_id);

      if (deletedRows === 0) {
        return res.status(404).json({ error: 'Todo item not found' });
      }

      res.json({ message: 'Todo item soft-deleted successfully' });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal server error' });
    }
  }
  
}


module.exports = TodoController;
