const request = require('supertest');
const express = require('express');
const app = require('../index');

const { TodoItem } = require('../models/todo');
const todoController = require('../controllers/todo');

app.get('/', todoController.getTodoItems);
app.get('/:list_id', todoController.getTodoItemById);
app.put('/:list_id', todoController.updateTodoItem);
app.post('/', todoController.createTodoItem);
app.delete('/:list_id', todoController.deleteTodoItem);
app.delete('/soft/:list_id', todoController.softdeleteTodoItem);


//TESTING GET
test ('should get all todo', (done) => {
    request(app)
    .get('/')
    .expect(200)
    .expect('Content-Type', /json/)
    .then(response => { //then : setelah permintaan HTTP selesai dan respon diterima
            done()
    }).catch(done)
    })

//TESTING GET BY ID
test('should get a specific todo item by ID', async () => {
    const response = await request(app).get('/1'); // Replace 1 with an actual list_id
    expect(response.status).toBe(200);
  });

//TESTING PUT
test('should update item using PUT', async () => {
    // Gantilah dengan ID yang valid dari item yang ingin Anda perbarui
    const todoId = 1;
    const updatedItem = {
      list_name: 'test saja ubah',
      description: 'test saja ubah',
    };
    const response = await request(app)
      .put(`/${todoId}`)
      .send(updatedItem);
    expect(response.status).toBe(200);
    });

// TESTING CREATE POST
test('should create a new todo item', async () => {
    const newItem = {
      list_name: 'Test doang',
      description: 'Test doang',
    };
    const response = await request(app)
      .post('/')
      .send(newItem);
    expect(response.status).toBe(201);
  });

  
//TESTING SOFTDELETE
test('should delete a specific todo item by ID', async () => {
    // Gantilah ID dengan ID yang valid
    const todoId = 1;
    const response = await request(app).delete(`/soft/${todoId}`);
    expect(response.status).toBe(200);
    // Lakukan pengujian lain sesuai kebutuhan
  });








///CATATAN LATIHAN SEBELUMNYa

// const app = require('../app')
// const request = require('supertest')

// test('get message success from list todo api', (done) => {
//     request(app)
//     .get('/api/v1/todo')
//     .expect('Content-Type', /json/) // mengharapkan respon berisi data JSON
//     .expect(200) //mengharapkan permintaan berhasil
//     .then(response => { //then : setelah permintaan HTTP selesai dan respon diterima
//         expect(response.body.message).toBe("Success")
//         done() //apakah pesan dalam respon sesuai dengan "Success"
//     })
//     .catch(done)
// })

// test('get count data from first page', (done) => {
//     request(app)
//     .get('/api/v1/todo')
//     .expect('Content-Type', /json/)
//     .expect(200)
//     .then(response => {
//         console.log(response);//ternyata bisa di LOG
//         expect(response.body.data.length).toBe(5)//data todolist harus 5
//     done()
//     })
// })


// test('get 1 data from list', (done) => {
//     request(app)
//     .get('/api/v1/todo')
//     .expect('Content-Type', /json/)
//     .expect(200)
//     .then(response => {
//         expect(response.body.data[0].title).toBe('Todo 1')
//         done()
//     })
// })