FROM node:lts-alpine3.18

WORKDIR /apitodo/src/app

COPY package*.json ./

COPY . .

EXPOSE 8080

CMD [ "node", "index.js" ]