const { Sequelize, DataTypes, Model } = require('sequelize');
const sequelize = new Sequelize('todo', 'postgres', 'mukti', {
  host: 'localhost',
  dialect: 'postgres'
});

class TodoItem extends Model {
  static async getTodoItems() {
    try {
      const query = 'SELECT * FROM todo_items';
      const results = await sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
      return results;
    } catch (error) {
      throw error;
    }
  }
  static async getTodoItemById(list_id) {
    try {
      const query = 'SELECT * FROM todo_items WHERE list_id = :list_id';
      const [item] = await sequelize.query(query, {
        replacements: { list_id },
        type: sequelize.QueryTypes.SELECT
      });

      return item;
    } catch (error) {
      throw error;
    }
  }
  static async createTodoItem(listName, description) {
    try {
      const query = 'INSERT INTO todo_items (list_name, description) VALUES (:listName, :description) RETURNING list_id';
      const [result] = await sequelize.query(query, {
        replacements: { listName, description },
        type: sequelize.QueryTypes.INSERT
      });

      const newID = result[0];
      return newID;
    } catch (error) {
      throw error;
    }
  }
  static async updateTodoItem(list_id, list_name, description) {
    try {
      const query = 'UPDATE todo_items SET list_name = :list_name, description = :description WHERE list_id = :list_id';
      const [updatedRows] = await sequelize.query(query, {
        replacements: { list_id, list_name, description },
        type: sequelize.QueryTypes.UPDATE
      });

      return updatedRows;
    } catch (error) {
      throw error;
    }
  }
  static async deleteTodoItem(list_id) {
    try {
      const query = 'DELETE FROM todo_items WHERE list_id = :list_id';
      const [deletedRows] = await sequelize.query(query, {
        replacements: { list_id },
        type: sequelize.QueryTypes.DELETE
      });

      return deletedRows;
    } catch (error) {
      throw error;
    }
  }
  static async softDeleteTodoItem(list_id) {
    try {
      const query = 'UPDATE todo_items SET status = :status WHERE list_id = :list_id';
      const [updatedRows] = await sequelize.query(query, {
        replacements: { list_id, status: 'deleted' },
        type: sequelize.QueryTypes.UPDATE,
      });

      return updatedRows;
    } catch (error) {
      throw error;
    }
  }
}

TodoItem.init(
  {
    list_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    list_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    description: {
      type: DataTypes.TEXT,
    },
    status: {
      type: DataTypes.STRING,
      defaultValue: 'active', 
    },
  },
  {
    sequelize,
    modelName: 'TodoItem',
    tableName: 'todo_items',
    timestamps: false,
  }
);

module.exports = {TodoItem, sequelize };