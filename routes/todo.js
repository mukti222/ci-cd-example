const express = require('express');
const router = express.Router();
const todoController = require('../controllers/todo');

// Routes /todo/
router.get('/', todoController.getTodoItems);
router.get('/:list_id', todoController.getTodoItemById);
router.post('/', todoController.createTodoItem);
router.put('/:list_id', todoController.updateTodoItem);
router.delete('/:list_id', todoController.deleteTodoItem);
router.delete('/soft/:list_id', todoController.softdeleteTodoItem);

module.exports = router;
